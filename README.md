# exim-cloudmark-analyzer
Tool that finds CMAE fingerprints in EXIM log files by parallel grep.

Specify search operand flag for SMTP or MX/POP/IMAP logs, email address and date.

Usage: 'cmaefinder.sh -f SMTP_PREFIX/MX_PREFIX -a "MAIL_ADDRESS" -d DATE "YYYYMMDD" -v (verbose)'

[PayPal.Me](https://paypal.me/jnick85?locale.x=nl_NL)